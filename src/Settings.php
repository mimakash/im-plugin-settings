<?php
namespace WPPlugines\Plugin;

/**
 * Exit if accessed directly
 * 
 * @since 1.0.0
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Main Plugin Class.
 *
 * @class Plugin
 */
class Settings {

    /**
     * The single instance of the class.
     *
     * @var Plugin
     * @since 2.1
     */
    protected static $_instance = null;

    /**
     * Main Plugin Instance.
     *
     * Ensures only one instance of Plugin is loaded or can be loaded.
     *
     * @since 2.1
     * @static
     * @return Plugin - Main instance.
     */
    public static function instance() {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Plugin Constructor.
     */
    public function __construct() {
        add_action( 'wp_head', [ $this, 'head' ] );
    }

    public function head() {
        echo "TTTTTTTTTTTTTTt";
    }
}

Settings::instance();